function monthInDate(date) {
    let month =  date.split('/');
    return month[1];
}

module.exports = monthInDate;
