function fullName(obj) {
    let str = [];
    for(let item in obj)
    {
        str.push(obj[item])
    }
    
    let lowerCase =  str.map(item => item.toLowerCase());
    return lowerCase.map(item => item.charAt(0).toUpperCase() + item.slice(1)).join(" ");
}


module.exports = fullName;