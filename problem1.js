
function value(number){

    return number.split("").filter(element => element !=="$").join("");
}


module.exports = value;